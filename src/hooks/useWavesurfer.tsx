import { useState, useEffect } from "react";
import WaveSurfer from "wavesurfer.js";

let instances: WaveSurfer[] = [];

interface WaveSurferHook {
  playing: boolean;
  togglePlay: () => void;
  seekTo: (percentage: number) => void;
}


let ELs = 0;

function useWaveSurfer(audioUrl: string, audioIndex: number): WaveSurferHook {
  const [waveSurfer, setWaveSurfer] = useState<WaveSurfer | null>(null);
  const [playing, setPlaying] = useState(false);
  // const windowSize = useWindowSize();

  useEffect(() => {
    const wavesurfer = WaveSurfer.create({
      container: `#waveform-${audioIndex}`,
      waveColor: "#424242",
      progressColor: "#0070f3",
    });

    wavesurfer.load(audioUrl); // * can load peaks in as second arg
    // * wavesurfer.load(audioUrl, [0.1, 0.2, 0.3, 0.4, 0.5]);

    // wavesurfer.load(
    //   'https://ia902606.us.archive.org/35/items/shortpoetry_047_librivox/song_cjrg_teasdale_64kb.mp3',
    //   peaks
    // );

    setWaveSurfer(wavesurfer);
    instances.push(wavesurfer);


    return () => {
      wavesurfer.destroy();
      instances.splice(instances.indexOf(wavesurfer), 1);
    };
  }, [audioUrl, audioIndex]);

  useEffect(() => {
    if (waveSurfer) {
      //console.log('export peaks', wavesurfer.backend.getPeaks(300, 0, 300))
      waveSurfer.on("play", () => setPlaying(true));
      waveSurfer.on("pause", () => setPlaying(false));
    }
    return () => {
      if (waveSurfer) {
        waveSurfer.un("play");
        waveSurfer.un("pause");
      }
    };
  }, [waveSurfer]);

  useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      const index = instances.findIndex(i => i.isPlaying())
      const instance = instances[index];

      switch (event.keyCode) {
        case 38: {
          // const isPlaying = instances.findIndex(i =>i.isPlaying())
          console.log("keyup", audioIndex);
          if (instance) {
            instance.stop();
            (instances[index - 1]) ? instances[index - 1].play() : instances[0].play()
          } else {
            instances[0].play()
          }
          break;
        }

        case 40: {
          //  const isPlaying = instances.findIndex(i =>i.isPlaying())
          console.log("keydown", audioIndex);
          if (instance) {
            instance.stop();
            // console.log('instance ' + audioIndex + " was playing")
           // console.log('play ', index + 1)
            (index+1 < instances.length) ? instances[index+1].play() : instances[instances.length-1].play()
          } else {
            instances[instances.length-1].play()
          }

          break;
        }

        default:
          break;
      }
    };
    
    // todo: must only be a single event listener on window.keyup 
    // todo for this else it wont work correctly. 
    // todo (fires all of them plus its not performant)
    if (ELs === 0) document.addEventListener("keydown", handleKeyDown);
    ELs = 1;
    return () => {
      if (ELs === 1) document.removeEventListener("keydown", handleKeyDown);
      ELs = 0;
    };
  }, [audioIndex]);

  const togglePlay = () => {
    console.log(instances);
    if (waveSurfer) {
      // instances[i].isPlaying() ? instances[i].stop() : instances[i].play()
      if (waveSurfer.isPlaying()) {
        waveSurfer.stop();
      } else {
        instances.forEach((i) => i.stop());
        waveSurfer.play();
      }
    }
  };

  const seekTo = (percentage: number) => {
    if (waveSurfer) {
      waveSurfer.seekTo(percentage);
    }
  };

  return { playing, togglePlay, seekTo };
}

export default useWaveSurfer;
