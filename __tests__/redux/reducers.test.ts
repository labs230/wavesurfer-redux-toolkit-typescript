import audioReducer, {
  setCurrentTrackMeta,
  addInstance,
  removeInstance,
  setCurrentPlayer,
  setPlayState,
  setTimeElapsed,
  setPercentageElapsed,
} from "../../src/features/audio/audioSlice";

describe("Redux Audio Reducer Tests", () => {
  const initialState = {
    currentPlayer: "",
    instances: [],
    isPlaying: false,
    percentageElapsed: 0,
    status: "idle" as const,
    timeElapsed: 0,
    trackMeta: { duration: 0, name: "" },
  };

  it("initialised redux is equal to initial state", () => {
    expect(audioReducer(initialState, { type: undefined })).toStrictEqual(
      initialState
    );
  });
  it("shoud set the current player to wave-7", () => {
    expect(audioReducer(undefined, setCurrentPlayer("wave-7"))).toStrictEqual({
      ...initialState,
      currentPlayer: "wave-7",
    });
  });
  it("shoud set the current track meta", () => {
    const testMeta = { name: "test track meta", duration: 60 };
    const newState = audioReducer(undefined, setCurrentTrackMeta(testMeta));
    expect(newState).toStrictEqual({ ...initialState, trackMeta: testMeta });
  });
  it("shoud add an instance", () => {
    const instance = "test-wave-0";
    const newState = audioReducer(
      undefined,
      addInstance({ instance, location: 0 })
    );
    expect(newState).toStrictEqual({ ...initialState, instances: [instance] });
  });

  it("shoud add an instance to existing instances at nth location", () => {
    const initialInstances = [
      "test-wave-0",
      "test-wave-1",
      "test-wave-2",
      "test-wave-3",
    ];
    const finalState = audioReducer(
      { ...initialState, instances: initialInstances },
      addInstance({ instance: "test-wave-add-at-index-2", location: 2 })
    );

    finalState

    // insert the test case into the 2nd index to use it as the correct value to test the redux state against
    initialInstances.splice(2, 0, "test-wave-add-at-index-2");

    expect(finalState).toStrictEqual({
      ...initialState,
      instances: initialInstances,
    });
  });

  it("shoud remove an instance", () => {
    const initialInstances = [
      "test-wave-0",
      "test-wave-1",
      "test-wave-2",
      "test-wave-3",
    ];
    const finalState = audioReducer(
      { ...initialState, instances: initialInstances },
      removeInstance("test-wave-1")
    );

    // remove instance at array index 1 to test against new redux state
    initialInstances.splice(1, 1);

    expect(finalState).toStrictEqual({
      ...initialState,
      instances: initialInstances,
    });
  });

  it("shoud set playstate", () => {
    const finalState = audioReducer(initialState, setPlayState(true));

    expect(finalState).toStrictEqual({
      ...initialState,
      isPlaying: true,
    });
  });
  it("shoud set timeElapsed and percentageElapsed", () => {
    expect(audioReducer(initialState, setTimeElapsed(60)).timeElapsed).toBe(60);

    expect(
      audioReducer(initialState, setPercentageElapsed(0.6)).percentageElapsed
    ).toBe(0.6);
  });
});
