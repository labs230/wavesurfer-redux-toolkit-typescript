// const HTMLMediaElement = class {

//     paused = false;
//     src = null;
    
//     constructor () {
//         this.paused = false;
//     }

//     load () {
//         console.log('loaded');
//     }
//     play () {
//         console.log('play');
//     }
//     pause () {
//         console.log('pause');
//     }

// }
      
// todo: doesnt work from here

// * Mock HTML Media / Audio element
global.window.HTMLMediaElement.prototype._mock = {
    paused: true,
    duration: NaN,
    _loaded: false,
     // Emulates the audio file loading
    _load: function audioInit(audio) {
      // Note: we could actually load the file from this.src and get real duration
      // and other metadata.
      // See for example: https://github.com/59naga/mock-audio-element/blob/master/src/index.js
      // For now, the 'duration' and other metadata has to be set manually in test code.
      audio.dispatchEvent(new Event('loadedmetadata'))
      audio.dispatchEvent(new Event('canplaythrough'))
    },
    // Reset audio object mock data to the initial state
    _resetMock: function resetMock(audio) {
      audio._mock = Object.assign(
        {},
        global.window.HTMLMediaElement.prototype._mock,
      )
    },
  }
  
  // Get "paused" value, it is automatically set to true / false when we play / pause the audio.
  Object.defineProperty(global.window.HTMLMediaElement.prototype, 'paused', {
    get() {
        console.log('check paused, returning', this._mock.paused)
      return this._mock.paused
    },
  })

  
  // Get and set audio duration
  Object.defineProperty(global.window.HTMLMediaElement.prototype, 'duration', {
    get() {
      return this._mock.duration
    },
    set(value) {
      // Reset the mock state to initial (paused) when we set the duration.
      this._mock._resetMock(this)
      this._mock.duration = value
    },
  })
  
  // todo: MOCK FUNCTION I ADDED
  global.window.HTMLMediaElement.prototype.load = function playMock() {
    // console.log('mock load')
    // this._mock.paused = true;
    this.dispatchEvent(new Event('onload'))
  }
  
  // Start the playback.
  global.window.HTMLMediaElement.prototype.play = function playMock() {
    console.log('mock play')
    // if (!this._mock._loaded) {
    //   // emulate the audio file load and metadata initialization
    //   this._mock._load(this)
    // }
    // if (this.paused) {
        // TODO: this needs a lot more work
        this.dispatchEvent(new Event('play'))
        this.dispatchEvent(new Event('playing'))
        this.dispatchEvent(new Event('progress'))
        this._mock.paused = false
    // } else {
    //     this.dispatchEvent(new Event('pause'))
    //     this._mock.paused = true
    // }
    // Note: we could
    return Promise.resolve(true);
  }
  
  // Pause the playback
  global.window.HTMLMediaElement.prototype.pause = function pauseMock() {
    console.log('mock pause')

    this.dispatchEvent(new Event('pause'))
       this._mock.paused = true
   
  }




export default global.window.HTMLMediaElement;