// import { render, screen } from "test-utils";
import audioHandler from "../../src/pages/api/audio";
import { IAudio } from "../../src/features/audio/archive-audioAPI";
// import "@testing-library/jest-dom";

import { createMocks, RequestMethod } from 'node-mocks-http';
import type { NextApiRequest, NextApiResponse } from 'next';

function mockRequestResponse(method: RequestMethod = 'GET', query = {}, headers = {}) {
  const {
    req,
    res,
  }: { req: NextApiRequest; res: NextApiResponse & { _getJSONData: () => unknown } } = createMocks({ method });
  req.headers = {
    'Content-Type': 'application/json',
    ...headers,
  };
  req.query = query;
  return { req, res };
}


// test lifecycle hooks
beforeAll(() => {
  // console.log("before all tests");
});
beforeEach(() => {
  // console.log("before each test");
});
afterEach(() => {
  // console.log("after each test");
});
afterAll(() => {
  // console.log("after all tests");
});

describe("Audio API Endpoint Tests", () => {

  it("returns audio data from the audio api endpoint using mock req & res", async () => {

    const { req, res } = mockRequestResponse();
    
    audioHandler(
      req,
      res
    );

    const data = res._getJSONData(); // short-hand for JSON.parse( response._getData() );

    expect(data).toMatchSnapshot();
    
  });

});

