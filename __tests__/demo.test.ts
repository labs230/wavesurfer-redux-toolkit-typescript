
const anApiCall = jest.fn();


it("the typed sum of 5 and 5 is 10", () => {
  const num1 = 5; // number inferred
  const num2 = 5; // number inferred
  expect(num1 + num2).toBe(10);

});

export {}

