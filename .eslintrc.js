module.exports = {
  extends: [
    "next/core-web-vitals",
    "prettier",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:testing-library/react"
  ],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: './tsconfig.json',
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: [
    "@typescript-eslint", 
    "testing-library",
    "plugin:jest/recommended",
    "plugin:jest/style",
],
  root: true,
  rules: {
    "no-restricted-imports": "off",

    "@typescript-eslint/no-restricted-imports": [
      "warn",
      {
        "name": "react-redux",
        "importNames": ["useSelector", "useDispatch"],
        "message": "Use typed hooks `useAppDispatch` and `useAppSelector` instead."
      }
    ],

    "testing-library/await-async-query": "error",
		"testing-library/no-await-sync-query": "error",
		"testing-library/no-debugging-utils": "warn",
		"testing-library/no-dom-import": "off",
    "testing-library/no-unnecessary-act": ["warn", { "isStrict": false }]
  },
  settings: {
    next: {
      rootDir: "src/"
    }
  }
}

