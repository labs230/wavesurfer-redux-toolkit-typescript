import { useEffect, useState } from "react";

export default function useOnScreen(ref, rootMargin = "0px") {
  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    if (ref.current == null) return;
    const observer = new IntersectionObserver(
      ([entry]) => {
        // console.log("intersection observer setIsVisible");
        if (ref.current == null) return;
        setIsVisible(entry.isIntersecting);
      },
      { rootMargin }
    );
    observer.observe(ref.current);
    const cleanupRef = ref.current;
    return () => {
      // if (ref.current == null) return
      if (cleanupRef == null) return;
      // console.log("destroy intersection observer");
      observer.unobserve(cleanupRef);
    };
  }, [ref, rootMargin]);

  return isVisible;
}
