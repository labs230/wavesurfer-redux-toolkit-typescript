import type { NextApiHandler, NextApiRequest, NextApiResponse } from "next";
import { IAudio } from "../../features/audio/archive-audioAPI";

const audioHandler: NextApiHandler<{ data: IAudio[] }> = async (
  request, //  NextApiRequest,
  response //  NextApiResponse<{ data: IAudio[] }>
) => {
  // const { amount = 1 } = request.body

  // simulate IO latency
  const audioData: IAudio[] = [
    {
      name: "Controlling Time",
      file: "/controlling-time.mp3",
      duration: 8,
      peaks: [],
    },
    {
      name: "Cutting Teeth",
      file: "/cutting-teeth.mp3",
      duration: 16,
      peaks: [],
    },
    {
      name: "Mix Sesh",
      file: "/mix-sesh.wav",
      duration: 32,
      peaks: [],
    },
    {
      name: "Moog Fun",
      file: "/moog-fun.wav",
      duration: 64,
      peaks: [],
    },
  ];

  return response.json({ data: audioData });
};

export default audioHandler;
