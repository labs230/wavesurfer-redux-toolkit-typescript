export interface IAudio {
  name: string;
  file: string;
  duration: number;
  peaks: number[];
}

export async function fetchAudio(): Promise<{ data: IAudio[] }> {
  const response = await fetch('/api/audio', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    // body: JSON.stringify(),
  })
  const result = await response.json() as Promise<{ data: IAudio[] }>

  return result 
}
