import { configureStore, combineReducers, type PreloadedState } from '@reduxjs/toolkit'

import counterReducer from './features/counter/counterSlice'
import audioReducer from './features/audio/audioSlice'

// Create the root reducer independently to obtain the RootState type
const rootReducer = combineReducers({
  counter: counterReducer,
  audio: audioReducer
})

export function setupStore(preloadedState?: PreloadedState<RootState>) {
  return configureStore({
    reducer: rootReducer,
    preloadedState
  })
}

const store = setupStore()

export type RootState = ReturnType<typeof rootReducer>
export type AppStore = ReturnType<typeof setupStore>
export type AppDispatch = AppStore['dispatch']
export default store
