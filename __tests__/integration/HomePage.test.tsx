import { render, screen, within, waitFor } from "../../test-utils";
import IndexPage from "../../src/pages/index";
import "@testing-library/jest-dom";
import { act } from "react-dom/test-utils";
import userEvent from "@testing-library/user-event";

import { data as AudioDataArray } from "../../src/pages";

// * Mock IntersectionObserver. it isn't available in test environment
const mockIntersectionObserver = () => {
  // console.log("mocking intersection observer");
  const mockIntersectionObserver = jest.fn();
  mockIntersectionObserver.mockReturnValue({
    observe: () => null,
    unobserve: () => null,
    disconnect: () => null,
  });
  window.IntersectionObserver = mockIntersectionObserver;
};

// extend global so we can mock bson-objectid package
declare global {
  // eslint-disable-next-line no-var
  var index: number;
  interface Window {
    index: number;
  }
}
window.index = 0;

// todo.... move this to mocks
const mockBsonObjectId = () => {
  // * mock from __mocks__ folder
  jest.mock("bson-objectid");

  // * mock inline with factory func
  // jest.mock("bson-objectid", () => {
  //   return function () {
  //     const objectIdClass = class {
  //       static toHexString = () => {
  //         if (typeof window === "undefined") {
  //           globalThis.index =
  //             globalThis?.index !== undefined ? (globalThis.index += 1) : 0;
  //           return globalThis.index;
  //         } else {
  //           window.index = window?.index !== undefined ? (window.index += 1) : 0;
  //           return window.index;
  //         }
  //       };
  //     };

  //     return objectIdClass;
  //   };
  // });
};

// mock isVisible hook
jest.mock("../../src/hooks/useOnScreen/useOnScreen", () => {
  return jest.fn(() => true);
});

// test lifecycle hooks
beforeAll(() => {
  // console.log("before all tests");
});
beforeEach(() => {
  window.index = 0;
  // console.log("before each test");
  mockIntersectionObserver();
  mockBsonObjectId();
});
afterEach(() => {
  // console.log("after each test");
});
afterAll(() => {
  // console.log("after all tests");
});

describe("Home", () => {
  it("renders the index page heading by test ID", async () => {
    await act(async () => { // * throws an err but been changed to a warning
    
    render(
      <IndexPage />
      );
       
    });
    
      const heading = await screen.findByTestId("main-heading");
      
      expect(heading).toBeInTheDocument();
  });

  it("renders the index page heading by text", async () => {
    render(<IndexPage />);
    const heading = await screen.findByText("Wavesurfer redux");
    expect(heading).toBeInTheDocument();
  });

  it("renders the index page heading by role of heading level 2 & text content", async () => {
    render(<IndexPage />);

    const heading = await screen.findByRole("heading", {
      level: 2,
      name: /Ws With Redux Audioplayers/i,
    });

    expect(heading).toBeInTheDocument();
  });

  it("checks paragraph test includes instructions", async () => {
    const utils = render(<IndexPage />);

    // console.log(utils)

    const {
      container,
      store,
      debug,
      unmount,
      rerender,
      asFragment,
      ...queries
    } = utils;

    const paraText =
      "use the arrow and spacebar keys to play the sounds or alternatively play all";

    const paragraph = await screen.findByText(paraText, { exact: false });

    // debug(paragraph)

    expect(paragraph).toHaveTextContent(
      /use the arrow and Spacebar keys to play the sounds/i
    );
  });

  it("checks that there are 14 audioplayers with play buttons on the screen", async () => {
    const { debug } = render(<IndexPage />);

    // debug entire container
    // debug();

    const buttons = await screen.findAllByRole("button", { name: "Play" });

    // debug buttons
    // debug(buttons);

    expect(buttons).toHaveLength(14);
  });

  it("does a snapshot test", async () => {
    // const bsonObjectId = jest.fn(() => 'not-wizard');

    const { container, debug } = render(<IndexPage />);

    // debug entire container
    // debug(container);

    expect(container).toMatchSnapshot();
  });


  it("does an integration test to check sticky footer works", async () => {
   
    const track1 = AudioDataArray[0];
    const track2 = AudioDataArray[1];

    const user = userEvent.setup();
    const { store, debug } = render(<IndexPage />);

    
    
    const playButtons = screen.getAllByRole("button", { name: "Play" });
    
    const playButton1 = playButtons[0]
    const playButton2 = playButtons[1]
  
    
    await user.click(playButton1);

    const currState1 = store.getState();
    
    const footer = screen.getByRole('contentinfo');

    // check component button state
    expect(playButton1).toHaveTextContent('Pause')

    // * check redux state
    console.log(currState1)
    expect(currState1.audio.currentPlayer).toBe('wave-1')
    expect(currState1.audio.isPlaying).toBe(true)
    expect(currState1.audio.trackMeta.name).toBe(track1.name)

    // check footer stop button
    const footerBtn = within(footer).getByRole('button')
    expect(footerBtn).toHaveTextContent('stop')

    // check footer title
    const footerTitle = within(footer).getByTestId('footer-current-track-title')
    expect(footerTitle).toHaveTextContent(track1.name);

  //   console.log('click play on wave 2')
  //   await user.click(playButton2);
  //  await user.click(playButton2); // todo: this is wrong, should be a play/pause toggle - issue with mocked HtmlMediaElement

    // debug(playButton2)

    // check component button state
    // expect(playButton1).toHaveTextContent('Play')

    // await waitFor(() => expect(playButton2).toHaveTextContent('Pause'))

    // * check redux state

    // const currState2 = store.getState();
    // console.log(currState2)
    // expect(currState2.audio.currentPlayer).toBe('wave-2')
    // expect(currState2.audio.isPlaying).toBe(true)
    // expect(currState2.audio.trackMeta.name).toBe(track2.name)

    // // check footer stop button
    // const updatedFooterBtn = within(footer).getByRole('button')
    // expect(updatedFooterBtn).toHaveTextContent('stop')

    // // check footer title
    // const updatedFooterTitle = within(footer).getByTestId('footer-current-track-title')
    // expect(updatedFooterTitle).toHaveTextContent(track2.name);

   // debug(footer)

    // todo cannot seem to get a second click to work, issue in mocked HtmlMediaElement

    // todo: duration is NaN

    // todo: test keyboard shortcuts

  });



});
