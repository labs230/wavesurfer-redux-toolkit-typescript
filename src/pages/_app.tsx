import "../styles/globals.css";

import { Provider } from "react-redux";
import type { AppProps } from "next/app";
// import { AudioContextProvider } from "../contexts/WavesurferContext";

import store from "../store";

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      {/* <AudioContextProvider> */}
        <Component {...pageProps} />
      {/* </AudioContextProvider> */}
    </Provider>
  );
}
