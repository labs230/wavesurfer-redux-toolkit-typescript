declare global {
  // eslint-disable-next-line no-var
  var index: number;
  interface Window {
    index: number;
  }
}

const factoryFunc = function () {

const objectIdClass = class {
    static toHexString = () => {
      if (typeof window === "undefined") {
        globalThis.index =
          globalThis?.index !== undefined ? (globalThis.index += 1) : 0;
        return globalThis.index;
      } else {
        window.index = window?.index !== undefined ? (window.index += 1) : 0;
        return window.index;
      }
    };
  };

  return objectIdClass;

}

export default factoryFunc;