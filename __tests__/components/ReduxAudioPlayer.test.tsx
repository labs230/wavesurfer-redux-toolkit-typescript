import { render, screen, waitFor } from "../../test-utils";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";

// component under test
import { ReduxAudioPlayer } from "../../src/features/audio/ReduxAudioPlayer";

import { data as AudioDataArray } from "../../src/pages";

const AudioData = AudioDataArray[0];

// Mock IntersectionObserver // todo: do this globally from mocks dir
const mockIntersectionObserver = () => {
  // console.log("mocking intersection observer");
  const mockIntersectionObserver = jest.fn();
  mockIntersectionObserver.mockReturnValue({
    observe: () => null,
    unobserve: () => null,
    disconnect: () => null,
  });
  window.IntersectionObserver = mockIntersectionObserver;
};

// test lifecycle hooks
beforeAll(() => {
  // console.log("before all tests");
  mockIntersectionObserver();
});
beforeEach(() => {
  // console.log("before each test");
});
afterEach(() => {
  // console.log("after each test");
});
afterAll(() => {
  // console.log("after all tests");
});

// mock isVisible hook
jest.mock("../../src/hooks/useOnScreen/useOnScreen", () => {
  return jest.fn(() => true);
});


// // mock media actions
// window.HTMLMediaElement.prototype.load = jest.fn(() => {
//   /* do nothing */
//   console.log("MOCKED MEDIA ELEMENT LOAD");
// });
// window.HTMLMediaElement.prototype.play = jest.fn(() => {
//   /* do nothing */
//   console.log("MOCKED MEDIA ELEMENT PLAY");
// });
// window.HTMLMediaElement.prototype.pause = jest.fn(() => {
//   /* do nothing */
// });
// window.HTMLMediaElement.prototype.addTextTrack = jest.fn(() => {
//   /* do nothing */
// });



/**
 * TODO: TEST SIMPLE REDUX COUNTER FUNCTION FIRST THEN COME BACK TO THIS AFTER MOCKING AUDIO
 * 
 * see audio mock in jest setup
 * 
 * TODO: figure out whether to try and test WS properly or mock it
 * TODO: test the redux actions
 */
 
describe("Redux Audio Player Tests", () => {
  it("should render a functional Redux Audio Player", () => {
    render(<ReduxAudioPlayer {...AudioData} />);
    expect(screen.getByTestId("wave-0")).toBeInTheDocument();
  });

  it("toggles play when clicked", async () => {
    const user = userEvent.setup();

    const { store, debug } = render(<ReduxAudioPlayer {...AudioData} />);

    const playButton = screen.getByRole("button", { name: "Play" });

    // check initial redux loaded state before play
    const st = store.getState();
    expect(st.audio.instances.length).toBe(1);
    expect(st.audio.currentPlayer).toBe("")
    expect(st.audio.isPlaying).toBeFalsy()
    expect(st.audio.timeElapsed).toBe(0)
    expect(st.audio.trackMeta.name).toBe("")

    // debug(playButton);

    // * click play button
    console.log('click play once to start playback')
    await user.click(playButton);

    // expect button to say 'pause' now that it is playing
    expect(playButton).toHaveTextContent('Pause')

    // check redux state is playing track
    const updatedSt = store.getState();
    expect(updatedSt.audio.instances.length).toBe(1);
    expect(updatedSt.audio.currentPlayer).toBe("wave-2")
    expect(updatedSt.audio.isPlaying).toBeTruthy()
    expect(updatedSt.audio.trackMeta.name).toBe(AudioData.name)

    console.log('click play again to pause playback')
    await user.click(playButton);

    // * click again to set audio back to paused
    await waitFor(() => expect(playButton).toHaveTextContent('Play'))



    // check redux state is in sync
    const finalSt = store.getState();
    expect(finalSt.audio.instances.length).toBe(1);
    expect(finalSt.audio.currentPlayer).toBe("wave-2")
    expect(finalSt.audio.isPlaying).toBeFalsy()
    expect(finalSt.audio.trackMeta.name).toBe(AudioData.name);

    // check wavesurfer has the current tracks peaks loaded
    const waveSurferInstance = globalThis.instances[0];
    const wsInstancePeaks = waveSurferInstance.backend.getPeaks(300, 0, 300);
    expect(wsInstancePeaks).toEqual(AudioData.peaks)
  });

  // todo: test redux reducer slices/action

  // it('toggles when spacebar is pressed');
  // it('toggles when next or prev arrow keys are pressed');

  // it('displays a waveform container');

  // // redux
  // it('updates in redux');
  // it('updates in the sticky footer bar');
  // it('toggles start/stop when interacting with sticky footer bar');

  // it('sticky footer bar is in sam state as component when component is interacted with');
});
