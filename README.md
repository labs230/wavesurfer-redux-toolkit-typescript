# TODO

- do all of this in TS!
- integrate jest functions in node DONE
- integrate jest functions in browser DONE
- integrate jest functions for react component DONE 
- do some snapshot tests DONE

- use react testing library for frontend components
- try some mocking funcs & async feches with msw
- create unit tests for redux (reducer/actions)?
- create integration tests for redux
- add storybook for components
- build out stories for audioplayer
- add tests within storybook

- MSW Example for api calls

- write an integration test for the index page that clicks a audioplayer and checks the footer