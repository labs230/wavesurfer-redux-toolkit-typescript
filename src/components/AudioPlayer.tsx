import useWaveSurfer from "../hooks/useWavesurfer";

export function AudioPlayer({ url, index } : { url: string, index: number }) {
  const { playing, togglePlay, seekTo } = useWaveSurfer(
    url,
    index
  );

  return (
    <div style={{ minWidth: '100%', position: 'relative' }}>
      <div id={`waveform-${index}`}></div>
      <button onClick={() => togglePlay()}>{playing ? "Pause" : "Play"}</button>
    </div>
  );
}

