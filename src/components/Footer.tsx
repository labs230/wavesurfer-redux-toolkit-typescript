import { current } from "@reduxjs/toolkit";
import { useState } from "react";
import WaveSurfer from "wavesurfer.js/src/wavesurfer";
import { useAppSelector, useAppDispatch } from "../hooks";

/**
 * Footer
 * * Not adding mute and volume functionality to keep UI clean, user can do it natively
 */

export default function Footer() {
  const {
    percentageElapsed,
    timeElapsed,
    isPlaying,
    instances,
    trackMeta,
    currentPlayer,
  } = useAppSelector((st) => st.audio);

  const handlePlayButtonClick = (): void => {
    const ws = getInstanceById(currentPlayer!);

    console.log("STICKY AUDIOPLAYER PLAY OR STOP BUTTON PRESSED!");

    if (ws) {
      ws.isPlaying() ? ws.stop() : ws.play();
    }
  };

  const handleSeek = (e: React.MouseEvent<HTMLElement>) => {
    const ws = getInstanceById(currentPlayer!);
    if (ws) {
      const skipLocation = e.clientX / window.innerWidth;
      ws.seekTo(skipLocation);
    }
  };

  return (
    <footer
      style={{
        transition: "all linear 0.2s",
        position: "fixed",
        bottom: currentPlayer ? 0 : "-50px",
        left: 0,
        width: "100%",
        color: "white",
        backgroundColor: "#303030",
        zIndex: 3,
      }}
    >
      <div
        style={{ height: "8px", backgroundColor: "#707070" }}
        onClick={handleSeek}
      >
        <div
          style={{
            transition: "all linear 0.1s",
            height: "8px",
            width: `${percentageElapsed}%`,
            backgroundColor: "limegreen",
          }}
        ></div>
      </div>
      {currentPlayer ? (
        <div
          style={{
            paddingTop: "0.3rem",
            paddingBottom: "0.3rem",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <button
            style={{ marginRight: "4rem" }}
            onClick={handlePlayButtonClick}
          >
            {isPlaying ? "stop" : "play"}
          </button>
          <p data-testid="footer-current-track-title">
            {
              trackMeta.name // TODO: Add legit name of track
            }
          </p>

          {/* <p>
        {fTimeMMSS(timeElapsed)}{" / "}{fTimeMMSS(trackMeta.duration)}
        </p> */}
        </div>
      ) : null}
    </footer>
  );
}

/**
 * Get Instance By Id
 * fetches a wavesurfer instance from the globalThis.instances array
 * @param {string} playerId - id of the player to get
 * @returns {WaveSurfer|undefined} wavesurfer object if found or undefined
 * @example const waveSurferInstance = getInstanceById(currentPlayerId)
 // todo: refactor code in ReduxAudioPlayer like this as well and put in a utils dir
 */
const getInstanceById = (playerId: string): WaveSurfer | undefined => {
  return globalThis.instances.find((i) => i.mediaContainer.id === playerId);
};

/**
 * FTimMMSS
 * Formats Time to MM:SS with padding & rounding
 * @param {number} time - time in seconds
 * @returns {string} time string in MM:SS format
 * @example const timeString = fTimeMMSS(80);
 * timeString = '01:20'
 */
const fTimeMMSS = (time: number): string => {
  const minutes = String(Math.floor(time / 60)).padStart(2, "0");
  const seconds = String(Math.floor(time % 60)).padStart(2, "0");
  return `${minutes}:${seconds}`;
};
