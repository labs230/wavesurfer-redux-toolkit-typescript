import * as React from "react";

import dynamic from "next/dynamic";
// import WaveSurfer from 'wavesurfer.js';
import ObjectID from "bson-objectid";
import WaveSurfer from "wavesurfer.js/src/wavesurfer";
import { WaveSurferParams } from "wavesurfer.js/types/params";
// import useWaveSurfer from '../hooks/useWavesurfer';

interface ProviderProps {
  count: number;
  setCount: React.Dispatch<React.SetStateAction<number>>; // (num: number) => number
  initInstance: ({ playerId, audioUrl }: { playerId: string, audioUrl: string }) => ({ instance: WaveSurfer | null, destroyFunc: () => void }),
  instances: WaveSurfer[],
  togglePlay: (instance: WaveSurfer | null) => void
}

export const AudioPlayerContext = React.createContext<ProviderProps>({
  count: 0,
  setCount: () => null,
  initInstance: (...args) => ({ instance: null, destroyFunc: () => undefined }),
  instances: [],
  togglePlay: () => undefined
});

type FcWithChildren = React.FunctionComponent<{ children: React.ReactNode }>;

const instances: WaveSurfer[] = [];

export const AudioContextProvider: FcWithChildren = ({ children }) => {

  const [count, setCount] = React.useState<number>(0);
;
  const ws = React.useRef<typeof WaveSurfer | null>(null);

  // initial fetch of the WaveSurfer Class / Library
  React.useEffect(() => {
    const LoadWaveSurfer = async () => {
        if (!ws.current) {
            console.log('load wavesurfer')
            const WaveSurferMod = await import('wavesurfer.js');
            ws.current = WaveSurferMod.default;
        }
    }
    LoadWaveSurfer();
  }, [])

//   const [instances, setInstances] = React.useState<WaveSurfer[]>([])

  const initInstance: ProviderProps['initInstance'] = ({ 
    playerId,
    audioUrl
}) => {
    console.log('Initialising instance');


    // const WaveSurferMod = await import('wavesurfer.js');
    // const WaveSurfer = WaveSurferMod.default;

    const instance = ws.current.create({ 
      container: `#waveform-${playerId}`,
      waveColor: "#424242",
      progressColor: "#0070f3",
    });

    instance.load(audioUrl);

    instances.push(instance);

    const destroyFunc = () => {
        console.log('destroying instance')
        instance.destroy();
        instances.splice(instances.indexOf(instance), 1);
      };

    return { instance, destroyFunc }
  }

  const togglePlay: ProviderProps['togglePlay'] = (instance) => {
    console.log('toggle play', instance)
    if (instance) {
        // console.log(instance, instances);
        if (instance.isPlaying()) {
          instance.stop();
          // dispatch(toggleCurrentPlayer(null))
        } else {
            instances.forEach(i => i.isPlaying() ? i.stop() : null)
            instance.play();
          // dispatch(toggleCurrentPlayer(playerId.toHexString()))
        }
        return instance;
      }
  }

  return (
    <AudioPlayerContext.Provider
      value={{
        count,
        setCount,
        instances,
        initInstance,
        togglePlay
      }}
    >
      {children}
    </AudioPlayerContext.Provider>
  );
};
