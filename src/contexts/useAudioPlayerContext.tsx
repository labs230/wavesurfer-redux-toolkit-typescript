import { useContext } from 'react';
import { AudioPlayerContext } from './WavesurferContext';

// ----------------------------------------------------------------------

const useAudioPlayerContext = () => {
    const context = useContext(AudioPlayerContext);

   // console.log('USE AUDIOPLAYER CONTEXT')
   
  if (!context) throw new Error('useAudioPlayerContext context must be use inside AudioPlayerProvider');

  return context;
};

export default useAudioPlayerContext