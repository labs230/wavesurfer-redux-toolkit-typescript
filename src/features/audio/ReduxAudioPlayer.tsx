import React, { useState, useEffect, useRef, useMemo } from "react";
// import { PayloadAction } from '@reduxjs/toolkit';
import WaveSurfer from "wavesurfer.js"; // * ws must be in a dynamic non-ssr component due to using webAudio API
import ObjectID from "bson-objectid";
// import { AppState } from "../../store";
import { useAppSelector, useAppDispatch } from "../../hooks";

import {
  setCurrentTrackMeta,
  addInstance,
  removeInstance,
  setCurrentPlayer,
  setPlayState,
  setTimeElapsed,
  setPercentageElapsed,
} from "./audioSlice";

import useOnScreen from "../../hooks/useOnScreen/useOnScreen";

declare global {
  // eslint-disable-next-line no-var
  var instances: WaveSurfer[];
}

if (!globalThis?.instances) globalThis.instances = [];

/**
 * @name ReduxAudioPlayer
 * @description 
 * 
 * Implementation of WaveSurfer as React component to display waveform along
 * with easy to use controls.
 * 
 * * all instances that are instantiated are held in globalThis.instances
 * * this component only works on client side
 * * using on server will get '_self is undefined' error due to the WebAudioApi's dependency on the window.
 * 
 * @param options options props for the setup & functionality of the component
 * @param options.name human readable name of the audio track
 * @param options.url url to the track on local filesystem or s3 (gets fetched)
 * @param options.peaks optional preloaded peaks data. if supplied this will allow waveforms to display instantly
 * @returns React Component
 */
export function ReduxAudioPlayer({
  name,
  url,
  peaks,
}: {
  name: string;
  url: string;
  peaks?: number[];
}) {
  const playerId = useRef<string>("wave-" + ObjectID().toHexString());
  const containerRef = useRef<HTMLDivElement | null>(null);
  const [ws, setWs] = useState<WaveSurfer | null>(null);
  const [playing, setPlaying] = useState<boolean>(false);

  // todo: use some sort of tooltip to let the user know about
  // todo: arrow and space preview keys (then set check to localstorage)
  // todo: the above should be a separate tooltip function that can be used elsewhere on the site
  // todo: refactor component so its just a waveform with hooks for buttons control
  const visible = useOnScreen(containerRef, "400px"); // todo: was 0px, investigate if margin is needed or if preloading works

  // console.log({ visible })
  const dispatch = useAppDispatch();
  const currentPlayer = useAppSelector((st) => st.audio.currentPlayer);

  useEffect(() => {
    // console.log(playerId.current, visible)
    if (containerRef.current && visible) {
      // create the instance
      const instance = WaveSurfer.create({
        container: containerRef.current,
        waveColor: "#7ac1ff",
        progressColor: "#2198ff",
        responsive: true,
        height: 50,
        hideScrollbar: true,
        backend: "MediaElement", // * defines how content is loaded in
        fillParent: true,
        pixelRatio: 1, // can be set to 1 for faster rendering
        normalize: true, // no
        partialRender: true, // use peak cache to improve rendering of large waveforms
      });
      // *'none' || 'metadata' | 'auto' with backend of MediaElement
      instance.load(url, peaks, "metadata"); // todo: also save & add duration data

      setWs(instance);

      // insert into array at correct position depending on where it is in the dom
      const location = Array.from(
        document.querySelectorAll(".waveform-container-visible")
      )
        .map((el) => el.id)
        .indexOf(playerId.current);

      dispatch(addInstance({ instance: playerId.current, location }));

      globalThis.instances.splice(location, 0, instance);

      // todo: change data fetch to a proper request

      // todo: create redux actions to manage global ws?

      // todo: refactor any code that gets or checks the globalThis.instances to refactored and reusable functions

      // todo: turn this into a useWaveform hook?

      const playerIdToRemove = playerId.current;

      return () => {
        // console.log("I AM THE DESTROY FUNC!");
        if (!instance?.isDestroyed) {
          // console.log("destroying ws");
          instance.destroy();
          dispatch(removeInstance(playerIdToRemove));
          globalThis.instances.splice(
            globalThis.instances.indexOf(instance), // location,
            1
          );
        }
      };
    }
  }, [dispatch, peaks, url, visible]);

  useEffect(() => {
    if (ws) {
      let debouncedTime = 0;
      ws.on("play", () => {
        console.log("on play");
        setPlaying(true);
        dispatch(setCurrentPlayer(playerId.current));
        dispatch(setPlayState(true));
        dispatch(
          setCurrentTrackMeta({
            name,
            duration: ws.getDuration(),
            // ...more data here
          })
        );
        if (containerRef.current) {
          containerRef.current.style.border = "solid red 2px";
          containerRef.current.style.backgroundColor = "#31313111";
        }
        // containerRef.current?.scrollIntoView({ behavior: 'auto', block: 'center' });
      });
      ws.on("pause", () => {
        console.log("on pause ", playerId?.current);
        setPlaying(false);
        dispatch(setPlayState(false));
        if (containerRef.current) {
          containerRef.current.style.border = "";
          containerRef.current.style.backgroundColor = "";
        }
      });
      ws.on("audioprocess", (time) => {
        const elapsed = +time.toFixed(1);
        if (elapsed !== debouncedTime) {
          // console.log(ws, ws.getCurrentTime(), ws.getDuration())
          const percentage = ws.getCurrentTime() / ws.getDuration();
          dispatch(setTimeElapsed(elapsed));
          dispatch(setPercentageElapsed(+(percentage * 100).toFixed(1)));
          debouncedTime = elapsed;
        }
      });
      ws.on("seek", (percentage: number) => {
        if (percentage === 0) {
          // todo: check if time is already set to 0 so we dont fire this off audioPlayers.length times
          dispatch(setTimeElapsed(0));
          dispatch(setPercentageElapsed(0));
          return;
        }
        console.log("seek", percentage, playerId.current);

        const startPos = +ws.backend.getPlayedTime().toFixed(0);
        // console.log("start pos: ", startPos, "percentage: ", percentage);
        dispatch(setTimeElapsed(startPos));
        dispatch(setPercentageElapsed(+(percentage * 100).toFixed(1)));
        dispatch(setCurrentPlayer(playerId.current));
        globalThis.instances.filter((i) => i !== ws).forEach((i) => i.stop());
        ws.play();
      });

      ws.on("finish", () => {
        dispatch(setTimeElapsed(0));
        dispatch(setPercentageElapsed(0));
      });
    }
    return () => {
      if (ws) {
        ws.un("play", () => console.log("removed play event listener"));
        ws.un("pause", () => console.log("removed pause event listener"));
        ws.un("audioprocess", () =>
          console.log("removed audioprocess event listener")
        );
        ws.un("seek", () => console.log("removed seek event listener"));
        ws.un("finish", () => console.log("removed finish event listener"));
      }
    };
  }, [ws]);

  useEffect(() => {
    // must be set for each component else it will break when components are added/removed from a page
    const handleKeyDown = (event: KeyboardEvent) => {
      switch (event.keyCode) {
        case 38: {
          console.log("arrow up key");

          // only if theres currently a player
          if (currentPlayer) {
            // get index of current player
            const currentIndex = globalThis.instances.findIndex(
              (i) => i.mediaContainer.id === currentPlayer
            );
            console.log({ currentPlayer, instances, currentIndex });

            globalThis.instances[currentIndex]?.stop(); // stop the player
            if (globalThis.instances[currentIndex - 1]) {
              globalThis.instances[currentIndex - 1].play();
            } else {
              dispatch(setTimeElapsed(0));
              dispatch(setPercentageElapsed(0));
            }
          } else {
            globalThis.instances[0]?.play();
          }
          break;
        }

        case 40: {
          console.log("arrow down key");

          // only if theres currently a player
          if (currentPlayer) {
            console.log("we have a current player..");
            // get index of current player
            const currentIndex = globalThis.instances.findIndex(
              (i) => i.mediaContainer.id === currentPlayer
            );
            console.log({ currentIndex });
            globalThis.instances[currentIndex]?.stop(); // stop the player
            if (globalThis.instances[currentIndex + 1]) {
              console.log("there is another one after this");
              globalThis.instances[currentIndex + 1].play();
            } else {
              dispatch(setTimeElapsed(0));
              dispatch(setPercentageElapsed(0));
            }
          } else {
            console.log("we dont have a current player, play the first one");
            globalThis.instances[0]?.play();
          }

          break;
        }
        case 32: {
          console.log("SPACEBAR");
          if (currentPlayer) {
            const currentIndex = globalThis.instances.findIndex(
              (i) => i.mediaContainer.id === currentPlayer
            );
            globalThis.instances[currentIndex]?.isPlaying()
              ? globalThis.instances[currentIndex]?.stop()
              : globalThis.instances[currentIndex]?.play();
          } else {
            globalThis.instances[0]?.isPlaying()
              ? globalThis.instances[0]?.stop()
              : globalThis.instances[0]?.play();
          }
          break;
        }

        default:
          break;
      }
    };

    document.addEventListener("keydown", handleKeyDown);

    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    };
  }, [currentPlayer]);

  const togglePlay = () => {
    // console.log("toggle play on ", playerId?.current, ws?.isPlaying());
    if (ws) {
      if (ws.isPlaying()) {
        ws.pause();
      } else {
        globalThis.instances
          .filter((i) => i !== ws)
          .forEach((i) => {
            i.stop();
          });
        ws.play();
      }
    }
  };

  const stop = () => {
    if (ws?.isReady) {
      ws.stop();
      dispatch(setPlayState(false));
      dispatch(setTimeElapsed(0));
      dispatch(setPercentageElapsed(0));
    }
  };

  return (
    <div style={{ minWidth: "100%", position: "relative" }}>
      <div
        className={`waveform-container${visible ? "-visible" : ""}`}
        ref={containerRef}
        id={playerId.current}
        data-testid={playerId.current}
      ></div>
      <button
        onMouseDown={(e) => e.preventDefault()}
        onClick={() => stop()}
      >{`|<-`}</button>
      <button
        onClick={() => togglePlay()}
        onMouseDown={(e) => e.preventDefault()}
      >
        {playing ? "Pause" : "Play"}
      </button>
    </div>
  );
}
